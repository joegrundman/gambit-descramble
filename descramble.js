const http = require('http')

// decoder :: Number, Number -> Number
//
// take an int and a key and return an int that has been descrambled
const decoder = (num, key) => (num + (256 - key)) % 256

// evalAcceptability :: Number -> Number
//
// Each character is returned a weight valuedepending on suitability as an english ascii character.
const evalAcceptability = (num) => {
	const chr = String.fromCharCode(num)
	if(/[A-Z]/.test(chr)) return 7
	else if(/[est]/.test(chr)) return 22
	else if (/[a-z ]/.test(chr)) return 20
	else if(/[0-9]/.test(chr)) return 3
	else if(/[\.\,\:\!\?\(\)\@]/.test(chr)) return 1
	else return 0
}

// decode :: [Number], [Number, Number, Number] -> [Number]
//
// Decode the cipher using the provided keys.
const decode = (cipher, keys) => {
	return cipher.map((el, i) => decoder(el, keys[i % 3]))
}

// evaluate :: [Number] -> [[Number],[Number],[Number]]
//
// go through cipher and try all 256 ascii combinations and evaulate
// for acceptability. add totals to index of results array
// index %3 sorts decoded into different arrays in results
// so we can find the a,b and c keys for the descramble algorithm
const evaluate = (cipher) => {
	let results = [new Array(256).fill(0), new Array(256).fill(0), new Array(256).fill(0)]
		
	cipher.forEach((el, index) => {
		for (let i = 0; i < 256; i++) {
			results[index % 3][i] += evalAcceptability(decoder(el, i))
		}
	})
	return results
}

// getDecodeKeys :: [[Number], [Number], [Number]] -> [Number, Number, Number] 
//
// from each property of Object (a, b, c) find the index with the highest value
// and return these cipher keys as array
const getDecodeKeys = (keys) => {
	const findIndexWithMostValid = 
		(indexOfHighestVal, currentVal, currentIndex, arr) => arr[indexOfHighestVal] < currentVal ? currentIndex : indexOfHighestVal	
	return keys.map(key => key.reduce(findIndexWithMostValid, 0))
}

// printSolution :: [Number], [Number] -> void
// decode cipher, convert to ascii and join to string, then log result
const printSolution = (cipher, keys) => {
	const result = decode(cipher, keys).map(el => String.fromCharCode(el)).join('')
	console.log(result)
}

// getCipher ::  String -> Promise
//
// Promise will on success scrape the html for the cipher, extract it and return it
const getCipher = (url) => {
	return new Promise((resolve, reject) => {
		http.get(url, response => {
			let body = ''
			response.on('data', d => body += d)
			response.on('end', () => { 
				 let cipher = body.toString().match(/<BR>([\d\s]+)/)[1].trim() 
				resolve(cipher)
			})
			response.on('error', er => reject(Error(er)))
		})		
	}) 
}

// run the decoding algorithm
console.time('solve cipher')

let originalCipher = []

getCipher("http://quiz.gambitresearch.com")
	.then(data => data.split(' ').map(d => parseInt(d)))
	.then(cipher => {
		originalCipher = cipher
		return evaluate(cipher)
	})
	.then(results => getDecodeKeys(results))
	.then(keys => printSolution(originalCipher, keys))
	.then(() => console.timeEnd('solve cipher')) // ~ 70ms
	.catch(er => console.log(er))

